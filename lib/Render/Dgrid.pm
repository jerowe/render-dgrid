package Render::Dgrid;

use 5.010000;
use strict;
use warnings;

require Exporter;
use Carp;
use Class::MOP;
use Data::Dumper;
use Tie::IxHash;
use Moose::Role;


our @ISA = qw(Exporter);

our $VERSION = '0.01';

has 'dgcolumn' => (
    is => 'rw',
    isa => 'HashRef',
    default => sub {return {}},
);

has 'dgdata' => (
    is => 'rw',
    isa => 'ArrayRef',
    default => sub {return []},
);

has 'pretty_json' => (
    is => 'rw',
    isa => 'Bool',
    default => 1,
);

has 'columns' => (
    is => 'rw',
    isa => 'ArrayRef',
);

has 'display_columns' => (
    is => 'rw',
    isa => 'ArrayRef',
);

#A key for which column is the id - in MongoDB this is _id. If its not _id and not set a unique ID will be added
has 'dgid' => (
    is => 'rw',
    isa => 'Str',
    default => 'id',
);

sub process{
    my $self = shift;

    $self->render_rows_as_hashref;
    $self->render_header_as_hashref;
}

sub render_rows_as_string{
    my $self = shift;

    return(to_json( $self->dgdata, { pretty => $self->pretty_json } ));
}

sub render_header_as_string{
    my $self = shift;

    return(to_json( $self->dgcolumn, { pretty => $self->pretty_json } ));
}

use namespace::autoclean;
1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Render::Dgrid - Base module for rendering ResultSets or MongoDB Collections to dgrid objects.

=head1 SYNOPSIS

    use Moose::Role;
    with  'Render::Dgrid';

=head1 DESCRIPTION


=head2 EXPORT

None by default.

=head1 Tags

=head2 process

Sets up the dgrid table data and columns from the resultset

=head2 dgcolumn

Column ref portion of dgrid
Perl like this

    my $columns = {columns => {first => "First Name", last => "Last Name", age => "Age"}};

Json like this

    columns: {
        first: "First Name",
        last: "Last Name",
        age: "Age"
    }

=head2 dgdata

Data ref portion of dgrid
Perl hash like this

    my $data = [ {first => "Bob", last => "Barker", age => 89}, {first => "Vanna", last => "White", age => 55}, {first => "Pat", last => "Sajak", age => 65} ];

Json hash like this

    var data = [ 
        { first: "Bob", last: "Barker", age: 89 },
        { first: "Vanna", last: "White", age: 55 },
        { first: "Pat", last: "Sajak", age: 65 }
    ];

=head2 pretty_json

Boolean value to print pretty json or regular json

=head2 dgid

Dgrid requires a unique id foreach row in the grid. It does not need to be displayed, but it does need to be there or else it will silently fail. The default is 'id', but it can be set to any value. If it is set to 'id', and there is no 'id' in the resultset/collection, a random id will be generated.

=head1 SEE ALSO

Render::Dgrid::MongoDB, Render::Dgrid::MongoDB, Render::TextCSV_XS, Render::TextCSV_XS::MongoDB, HTML::FormHandler

=head1 AUTHOR

Jillian Rowe, E<lt>jillian.e.rowe@gmail.com<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Jillian Rowe

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.16.3 or,
at your option, any later version of Perl 5 you may have available.


=cut
