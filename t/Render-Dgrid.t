# Before 'make install' is performed this script should be runnable with
# 'make test'. After 'make install' it should work as 'perl Render-Dgrid.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

package MyTable;
use strict;
use warnings;

use Test::More tests => 1;

BEGIN { push @INC, '/home/jillian/perlmodules/Render/lib' };
BEGIN { push @INC, '/home/jillian/perlmodules/Render-Dgrid/lib' };

BEGIN { use_ok('Render::Dgrid') };

use Moose;
extends 'Render';
with 'Render::Dgrid';

my $dt = MyTable->new();
my $cl;
ok($cl = $dt->can('process'), 'cando process');

done_testing();
